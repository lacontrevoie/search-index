import * as express from 'express'
import { check } from 'express-validator'
import { areValidationErrors } from './utils'

const paginationValidator = [
  check('start').optional().isInt({ min: 0 }).withMessage('Should have a number start'),
  check('count').optional().isInt({ min: 0 }).withMessage('Should have a number count'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (areValidationErrors(req, res)) return

    return next()
  }
]

// ---------------------------------------------------------------------------

export {
  paginationValidator
}

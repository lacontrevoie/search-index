export interface ServerConfig {
  searchInstanceName: string
  searchInstanceNameImage: string

  searchInstanceSearchImage: string

  indexedHostsCount: number

  indexedInstancesUrl: string

  legalNoticesUrl: string
}

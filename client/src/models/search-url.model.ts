export interface SearchUrl {
  [id: string]: string | undefined | number[] | string[]

  search?: string
  nsfw?: string
  publishedDateRange?: string
  durationRange?: string
  categoryOneOf?: number[]
  licenceOneOf?: number[]
  languageOneOf?: string[]

  tagsAllOf?: string[]
  tagsOneOf?: string[]
}

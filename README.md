# Application behind search.joinpeertube.org

## Dev

```terminal
$ git submodule update --init --recursive
$ yarn install --pure-lockfile
```

The database (Elastic Search) is automatically created by PeerTube at startup.

Run simultaneously (for example with 3 terminals):

```terminal
$ tsc -w
```

```terminal
$ node dist/server
```

```
$ cd client && npm run serve
```

Then open http://localhost:8080.

## Production

Install dependencies:
  * NodeJS (v12)
  * Elastic Search

In the root of the cloned repo:

```terminal
$ git submodule update --init --recursive
$ yarn install --pure-lockfile
$ npm run build
$ cp config/default.yaml config/production.yaml
$ vim config/production.yaml
$ node dist/server.js
```
